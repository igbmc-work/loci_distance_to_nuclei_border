// Ask user to define input directory
InputDirName = getDirectory("Source Directory with images");
// Ask user to define output directory
OutputDirName = getDirectory("Destination Directory for output");

// Get list of files in folder
fileList = getFileList(InputDirName);

// Loop through images
for (i=0; i<fileList.length; i++) {
	// Only process it if it's a tif image
	if (endsWith(fileList[i], ".tif")) {
		// Open image and get its name
		open(InputDirName + fileList[i]);
		imgName = getTitle();
		// Assume all channels are together in a hyperstack --> split channels into separate images
		run("Split Channels");
		selectImage("C1-" + imgName);
		// Create MAX Intensity projection for spot detection
		run("Z Project...", "projection=[Max Intensity] all");
		// Small gaussian blur to smooth noise
		run("Gaussian Blur...", "sigma=1 stack");
		// close rest of images
		close("C1-" + imgName);
		close("C2-" + imgName);
		close("C3-" + imgName);
		// Assume the tracked labels are in the output folder from previous step
		open(OutputDirName + File.getNameWithoutExtension(imgName) + "_labels_tracked.tif");
		LabelImgName = getTitle();
		// Get number of frames of the current opened image
		selectImage("MAX_C1-" + imgName);
		nFrames = nSlices();
		// Loop through each frame
		for (i=1; i<=nFrames; i++) {
			// We want to convert the label image to ImageJ ROIs for each frame
			selectImage(LabelImgName);
			// Set the image to current frame we are working on
		    setSlice(i);
		    // Extract that frame by duplicating it as a separate image
		    run("Duplicate...", "title=" + "labels" + i);
		    // Convert label image to ROIs --> required BIOP plugin
		    run("Label image to ROIs", "rm=[RoiManager[size=105, visible=true]]");
			// Extract the working frame from the puncta channel as well
		    selectImage("MAX_C1-" + imgName);
		    setSlice(i);
		    run("Duplicate...", "title=" + "img" + i);
		    // Find all loci spot coordinates on the current frame using a find maxima approach
		    FindSpotsAndPlaceOntopRoiManager("img" + i);
    		// Find shortest distance between every locus and corresponding nucleus border ROI
		    FindShortestLinesBetweenPointsAndBorders(); 
		    // Export ROIs for found lines  
		    roiManager("Save", OutputDirName + File.getNameWithoutExtension(imgName) + "_ROIs_frame_" + i + ".zip");
		    // Calculate distances of each line in the ROI Manager and export it
	        ExportDistancesToNucleiBorder(OutputDirName, File.getNameWithoutExtension(imgName) + "_frame_" + i);
	        // Calculate shapes, area and diameter for each nuclear ROIs and export it
	        ExportNucleiMorphometrics(OutputDirName, File.getNameWithoutExtension(imgName) + "_frame_" + i);
	        // Close what we don't need
		    close("img" + i);
		    close("labels" + i);
		    close("ROI Manager");
		}
		// Close rest of stuff still open
		close(LabelImgName);
		close(imgName);
		close("MAX_C1-" + imgName);
	}
}

function ExportNucleiMorphometrics(outputdir, imgName){
	run("Set Measurements...", "area shape feret's display redirect=None decimal=3");
	ROIsToMeasure = findRoisWithName("0001");
	roiManager("select", ROIsToMeasure);
	roiManager("measure");
	saveAs("Results", outputdir + File.getNameWithoutExtension(imgName) + "_nuclei_morphometrics.csv");
    if (isOpen("Results")) {
     selectWindow("Results"); 
     run("Close" );
    }
}

function ExportDistancesToNucleiBorder(outputdir, imgName) { 

	roiName = "line";	
	nR = roiManager("Count"); 
	roiIdx = newArray(nR); 
	k=0; 
	clippedIdx = newArray(0); 
	
	for (i=0; i<nR; i++) { 
		roiManager("Select", i); 
		rName = Roi.getName(); 
		if ( startsWith(rName, roiName) ) { 
			roiIdx[k] = i; 
			k++; 
		} 
	} 
	if (k>0) { 
		clippedIdx = Array.trim(roiIdx,k); 
	}
	
	roiManager("select", clippedIdx);
	roiManager("show all");
	roiManager("Measure");
	
	saveAs("Results", outputdir + File.getNameWithoutExtension(imgName) + "_distances.csv");
    if (isOpen("Results")) {
     selectWindow("Results"); 
     run("Close" );
    }
}

function FindSpotsAndPlaceOntopRoiManager(imgName) { 
	// use find maxima to get multi-point selection of loci in image
	// add the multi-point selection to the top of the ROI manager by renaming it and sorting the ROIs --> needed for the FindShortestLinesBetweenPointsAndBorders function
	selectImage(imgName);
	run("Find Maxima...", "prominence=10 output=[Point Selection]");
	roiManager("Add");
	n = roiManager("count");
	roiManager("Select", n - 1);
	roiManager("rename", "0000 - Points");
	roiManager("sort");
}

function FindShortestLinesBetweenPointsAndBorders() { 
	// function adapted from https://github.com/ved-sharma/Shortest_distance_between_objects
	width = getWidth();
	height = getHeight();
	if(roiManager("count") < 2)
		exit("The macro needs at least 2 entries in the ROI Manager");
	
	strokeWd = 1;
	strokeCol = "blue";
	interval = 1;
	exclude = false;
	
	time0 = getTime();
	roiManager("select", 0);
	getSelectionCoordinates(x, y);
	len1 = x.length;
	count1 = 0; // count of cells close to the image edges
	count2 = 0; // count of cells which are inside 2D areas
	
	n = roiManager("count");
	roiManager("select", 1);
	run("Interpolate", "interval=&interval adjust");
	roiManager("update");
	getSelectionCoordinates(u, v);
	for(k=2; k<n; k++) {
		roiManager("select", k);
		run("Interpolate", "interval=&interval adjust");
		roiManager("update");
		getSelectionCoordinates(u1, v1);
		u = Array.concat(u,u1);
		v = Array.concat(v,v1);
	}
	len2 = u.length;
	roiNames = newArray();
	for(j=0; j<len1; j++) {
		if(pointInsideArea(j)) { 		// only consider points that are INSIDE corresponding ROI
			roiName = split(Roi.getName, " - ");
			roiNames = Array.concat(roiNames,roiName[2]);
			distFinal = sqrt((u[0]-x[j])*(u[0]-x[j]) + (v[0]-y[j])*(v[0]-y[j]));
			uFinal = u[0];
			vFinal = v[0];
			for(i=1; i<len2; i++) {
				dist = sqrt((u[i]-x[j])*(u[i]-x[j]) + (v[i]-y[j])*(v[i]-y[j]));
				if(dist < distFinal) {
					distFinal = dist;
					uFinal = u[i];
					vFinal = v[i];
				}
			}
			if(exclude) {
				if(!closeToBoundary(j)) {
					makeLine(x[j], y[j], uFinal, vFinal);
					Roi.setStrokeColor(strokeCol);
					Roi.setStrokeWidth(strokeWd);
					roiManager("add");
					roiManager("Select", roiManager("count")-1);
					roiManager("Rename", "line" + j+1 + "_" + currentRoiName);
				}
				else
					count1++; // counting the no. of cells which are too close to the image edge
			}
			else {
				makeLine(x[j], y[j], uFinal, vFinal);
				Roi.setStrokeColor(strokeCol);
				Roi.setStrokeWidth(strokeWd);
				roiManager("add");
				roiManager("Select", roiManager("count")-1);
				if (roiNames.length < j+1) {
					currentRoiName = roiNames[roiNames.length - 1];
				} else {
					currentRoiName = roiNames[j];
				}
				roiManager("Rename", "line" + j+1 + "_" + currentRoiName);
			}
		}
	}
}

function pointInsideArea(a) {
	for(i=1; i<n; i++) { // a and i local variables; n, x and y global
		roiManager("select", i);
		if(Roi.contains(x[a], y[a])) {
			count2++; // counting the no. of cells inside 2D areas
			return true;
		}
	}
	return false;
}

function closeToBoundary (a) {
	if(x[a] < distFinal)
		return true;
	else if ((width - x[a]) < distFinal)
		return true;
	else if (y[a] < distFinal)
		return true;
	else if ((height - y[a]) < distFinal)
		return true;
	else
		return false;
}

function findRoisWithName(roiName) { 
	// function adapted from https://forum.image.sc/t/selecting-roi-based-on-name/3809
	nR = roiManager("Count"); 
	roiIdx = newArray(nR); 
	k=0; 
	clippedIdx = newArray(0); 
	 
	for (i=0; i<nR; i++) { 
		roiManager("Select", i); 
		rName = Roi.getName(); 
		if (startsWith(rName, roiName) ) { 
			roiIdx[k] = i; 
			k++; 
		} 
	} 
	if (k>0) { 
		clippedIdx = Array.trim(roiIdx,k); 
	} 
	 
	return clippedIdx; 
} 